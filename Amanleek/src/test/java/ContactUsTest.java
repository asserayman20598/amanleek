import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ContactUsTest extends TestBase{
    @Test
    public void ValidContactUsTC() throws InterruptedException {
        new HomePage(driver)
                .ClickingOnContactUsButton()
                .enterUserName("Test")
                .enterUserEmail("Test@test.com")
                .enterUserPhoneNumber("+201001234567")
                .enterUserInsuranceType("Car insurance")
                .enterUserMsg("Test")
                .clickOnSendButton();
        Thread.sleep(1000);
        String Actual = driver.findElement(By.className("wpcf7-response-output")).getText();
        String Expected = "Your message has been sent, our team will contact you shortly.";
        Assert.assertEquals(Actual,Expected);
    }
}
