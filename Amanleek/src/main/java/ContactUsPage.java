import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ContactUsPage {
    private WebDriver driver;
    public ContactUsPage (WebDriver driver)
    {
        this.driver = driver;
    }
    private By nameField = By.name("yourname");
    private By emailField = By.name("email");
    private By phoneNumberField = By.name("tel-672-cf7it-national");
    private By insuranceTypeField = By.id("insurance-type-en");
    private By msgField = By.name("message");
    private By sendButton = By.xpath("//input[@type='submit']");

    public ContactUsPage enterUserName(String name)
    {
        Utility.enterText(driver,nameField,name);
        return new ContactUsPage(driver);
    }

    public ContactUsPage enterUserEmail(String email)
    {
        Utility.enterText(driver,emailField,email);
        return new ContactUsPage(driver);
    }
    public ContactUsPage enterUserPhoneNumber(String pNumber)
    {
        Utility.enterText(driver,phoneNumberField,pNumber);
        return new ContactUsPage(driver);
    }
    public ContactUsPage enterUserInsuranceType(String ins)
    {
        Utility.enterText(driver,insuranceTypeField,ins);
        return new ContactUsPage(driver);
    }
    public ContactUsPage enterUserMsg(String msg)
    {
        Utility.enterText(driver,msgField,msg);
        return new ContactUsPage(driver);
    }
    public ContactUsPage clickOnSendButton()
    {
        Utility.clicking(driver,sendButton);
        return new ContactUsPage(driver);
    }
}
