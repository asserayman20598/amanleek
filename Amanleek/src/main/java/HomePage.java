import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {
    private WebDriver driver;
    public HomePage (WebDriver driver)
    {
        this.driver = driver;
    }
    private By contactUsButton = By.xpath("//a[@href='https://amanleek.com/en/contact/']");


    public ContactUsPage ClickingOnContactUsButton()
    {
        Utility.clicking(driver,contactUsButton);
        return new ContactUsPage(driver);
    }
}
